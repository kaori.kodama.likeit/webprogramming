package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();

			String sql="SELECT * FROM user WHERE login_id=? and password=?";
			PreparedStatement pStmt=conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs=pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String loginIdDate=rs.getString("login_id") ;
			String nameDate=rs.getString("name");
			return new User(loginIdDate,nameDate);

		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {

			if(conn !=null) {
				try {
					conn.close();
				}catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//パスなくても更新可
	public User findByLogin(String loginId) {
		Connection conn=null;
		try {
			conn=DBManager.getConnection();

			String sql="SELECT * FROM user WHERE login_id=?";
			PreparedStatement pStmt=conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs=pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			String loginIdDate=rs.getString("login_id") ;
			String name=rs.getString("name") ;
			return new User(loginIdDate,name);

		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		}finally {

			if(conn !=null) {
				try {
					conn.close();
				}catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}



public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        // TODO: 未実装：管理者以外を取得するようSQLを変更する
        String sql = "SELECT * FROM user";

         // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int id = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

            userList.add(user);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;
}


public void insert(String loginId, String password, String username, String birth) {
	Connection conn=null;
	try {
		conn=DBManager.getConnection();

		String sql="insert into user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now())";
		PreparedStatement pStmt=conn.prepareStatement(sql);
		pStmt.setString(1, loginId);
		pStmt.setString(2, username);
		pStmt.setString(3, birth);
		pStmt.setString(4, password);

		int rs=pStmt.executeUpdate();


	} catch(SQLException e) {
		e.printStackTrace();
	}finally {

		if(conn !=null) {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}



public User findDetail(String id) {
    Connection conn = null;

	try {
		conn=DBManager.getConnection();

		String sql="SELECT * FROM user WHERE id = ?";
		PreparedStatement pStmt=conn.prepareStatement(sql);
		pStmt.setString(1, id);
		ResultSet rs=pStmt.executeQuery();

		if(!rs.next()) {
			return null;
		}

		String loginIdDate=rs.getString("login_id") ;
		String nameDate=rs.getString("name");
		Date birthDate=rs.getDate("birth_date");
		String createDate=rs.getString("create_date");
		String updateDate=rs.getString("update_date");
		return new User(loginIdDate,nameDate,birthDate,createDate,updateDate);

	} catch(SQLException e) {
		e.printStackTrace();
		return null;
	}finally {

		if(conn !=null) {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}


public void update (String loginId, String password, String username, String birth) {
	Connection conn=null;
	try {
		conn=DBManager.getConnection();

		String sql="UPDATE user SET name = ?, birth_date=?,password=? WHERE login_id=?";
		PreparedStatement pStmt=conn.prepareStatement(sql);

		pStmt.setString(1, username);
		pStmt.setString(2, birth);
		pStmt.setString(3, password);
		pStmt.setString(4, loginId);

		int rs=pStmt.executeUpdate();


	} catch(SQLException e) {
		e.printStackTrace();
	}finally {

		if(conn !=null) {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}

public void update (String loginId,String username, String birth) {
	Connection conn=null;
	try {
		conn=DBManager.getConnection();
		String sql="UPDATE user SET name = ?, birth_date=? WHERE login_id=?";
		PreparedStatement pStmt=conn.prepareStatement(sql);
		pStmt.setString(1, username);
		pStmt.setString(2, birth);
		pStmt.setString(3, loginId);

		int rs=pStmt.executeUpdate();


	} catch(SQLException e) {
		e.printStackTrace();
	}finally {

		if(conn !=null) {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}



public void delete (String loginId) {
	Connection conn=null;
	try {
		conn=DBManager.getConnection();

		String sql="DELETE FROM user WHERE login_id=?";
		PreparedStatement pStmt=conn.prepareStatement(sql);


		pStmt.setString(1, loginId);

		int rs=pStmt.executeUpdate();


	} catch(SQLException e) {
		e.printStackTrace();
	}finally {

		if(conn !=null) {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	}

public String seacret(String password) {

	//ハッシュを生成したい元の文字列
	String source = password;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";


	byte[] bytes = null;
	try {
		bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	} catch (NoSuchAlgorithmException e) {

		e.printStackTrace();
	}
	String result = DatatypeConverter.printHexBinary(bytes);

	System.out.println(result);
	return result;

}


public List<User> ichiran (String loginId, String name,String startDate,String endDate) {
	Connection conn=null;
	List<User> userList=new ArrayList<User>();
	try {
		conn=DBManager.getConnection();

		String sql="SELECT * FROM user WHERE login_id not in ('admin')";

		//loginIdが入力されてたら変数sqlに条件を追加
		if(!loginId.equals("")) {
			sql+=" AND login_id= '"+loginId+"'";
		}
		//nameが入力されてたら変数sqlに条件を追加
		if(!name.equals("")) {
			sql+=" AND name LIKE '%" + name + "%'";
		}

		if(!startDate.equals("")) {
			sql+=" AND birth_date >= '"+startDate+"'";
		}

		if(!endDate.equals("")) {
			sql+=" AND birth_date<='"+endDate+"'";
		}


		PreparedStatement pStmt=conn.prepareStatement(sql);

		ResultSet rs=pStmt.executeQuery();


        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
		 while (rs.next()) {
	            int id = rs.getInt("id");
	            String loginId1 = rs.getString("login_id");
	            String name1 = rs.getString("name");
	            Date birthDate = rs.getDate("birth_date");
	            String password = rs.getString("password");
	            String createDate = rs.getString("create_date");
	            String updateDate = rs.getString("update_date");
	            User user = new User(id, loginId1, name1, birthDate, password, createDate, updateDate);

	            userList.add(user);
	        }


	} catch(SQLException e) {
		e.printStackTrace();
	}finally {

		if(conn !=null) {
			try {
				conn.close();
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	return userList;


}
}





