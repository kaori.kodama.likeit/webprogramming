package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Sinki
 */
@WebServlet("/Sinki")
public class Sinki extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sinki() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		  request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String pass = request.getParameter("pass");
		String username = request.getParameter("username");
		String birth = request.getParameter("birth");



		UserDao userDao = new UserDao();


        //未入力
		if(loginId.equals("")||password.equals("")||pass.equals("")||username.equals("")| birth.equals("")) {

			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
			dispatcher.forward(request, response);
			return;

		}
		//確認パスワード違う場合
		if(!password.equals(pass)) {
			request.setAttribute("message", "パスワードと確認パスワードが違います");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
			dispatcher.forward(request, response);
			return;

		}

		User user = userDao.findByLogin(loginId);


		if (user != null) {
			request.setAttribute("message", "違うIDにしてください");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sinki.jsp");
			dispatcher.forward(request, response);
			return;
		}



		//暗号化呼び出し

		String a=userDao.seacret(password);


		userDao.insert(loginId, a, username, birth);



		response.sendRedirect("UserListServlet");
		}
	}


