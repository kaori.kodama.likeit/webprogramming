package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Update
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("id");


		UserDao model = new UserDao();

		User findDetail = model.findDetail(id);

		request.setAttribute("user", findDetail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String pass = request.getParameter("pass");
		String username = request.getParameter("username");
		String birth = request.getParameter("birth");

		//空白
		if (loginId.equals("") || username.equals("") | birth.equals("")) {
			User user = new User(loginId, username);
			//消えないように
			request.setAttribute("user", user);

			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードと確認パスワードが違ったら
		if (!password.equals(pass)) {
			request.setAttribute("message", "入力された内容は正しくありません");
			RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher1.forward(request, response);
			return;
		}

		UserDao userDao = new UserDao();

		String a = userDao.seacret(password);

		//パスワードがなくても更新される
		if (password.equals("") && pass.equals("")) {
			userDao.update(loginId, username, birth);
		} else {
			userDao.update(loginId, a, username, birth);
		}

		response.sendRedirect("UserListServlet");

	}

}
