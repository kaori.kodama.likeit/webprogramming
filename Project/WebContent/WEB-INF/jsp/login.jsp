<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
    <meta charset="UTF-8">
    <title>title</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="Content-Style-Type" content="text/css">


    <style type="text/css">

    </style>
</head>

<body>
    <table width="100%">
        <tr>
            <td align="center">
                <h1>ログイン画面</h1>
            </td>
        </tr>
    </table>
    <div>
	<c:if test="${message != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${message}
		</div>
	</c:if>

     <form class="form-signin" action="LoginServlet" method="post">
    <table class="mx-auto mt-5">
        <tr height="50">
            <td width="200">ログインID</td>
            <td><input name="loginId"></td>
        </tr>
        <tr>
            <td>パスワード</td>
            <td><input name="password"></td>
        </tr>
    </table>
        <div class="col-1 mx-auto mt-5">



          <input type="submit" value="ログイン" class="btn btn-primary">

         </form>
        </div>
    </div>



    <!--
            <table width="100%">

                <div class="row">
                    <td align="center"> ログインID <input type="text" style="width:200px;" class="form-control"></td>
                </div>


                <table width="100%">
                    <div class="row">

                        <td align="center"> パスワード <input type="text" style="width:200px;" class="form-control"></td>
                    </div>

                    <table width="100%">
                        <td align="center"> <input type="button" value="ログイン" class="btn btn-primary"></td>

                    </table>
                </table>

            </table>
-->



</body></html>
